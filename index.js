require('dotenv').config();

const ARRAY_1 = process.env.ARRAY_1.split(',');
const ARRAY_2 = process.env.ARRAY_2.split(',');

let newArray = [];

ARRAY_1.forEach(el => {
    if(newArray.findIndex((element) => element === el) === -1) 
    {
        newArray.push(el)
    }
});

ARRAY_2.forEach(el => {
    if(newArray.findIndex((element) => element === el) === -1) 
    {
        newArray.push(el)
    }
});

console.log(newArray);
